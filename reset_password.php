<?php

require_once 'connection.php';
require_once 'login/login_functions.php';

session_start();

if (isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

if (!isset($_GET['token']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

$errors = [];

$stmt = $conn->prepare('SELECT * FROM usuarios WHERE token = :token');
$stmt->bindValue(':token', $_GET['token']);
$stmt->execute();
$usuario = $stmt->fetch();
$user_id = $usuario['id'];
$invalid = ! $usuario || empty($usuario['token']) || link_has_expired($usuario['token_enviado_em']);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !$invalid)
{
  $nova_senha = isset($_POST['nova_senha']) ? trim($_POST['nova_senha']) : null;
	$confirmar_senha = isset($_POST['confirmar_senha']) ? trim($_POST['confirmar_senha']) : null;

	if (empty($confirmar_senha) || empty($nova_senha))
	{
		$errors[] = 'É obrigatório o preenchimento de ambos os campos.';
	}

	if ($confirmar_senha != $nova_senha)
	{
		$errors[] = 'As senhas digitadas não são iguais.';
	}

	if (empty($errors))
	{
		$stmt = $conn->prepare('UPDATE usuarios SET senha = :senha, token = NULL, token_enviado_em = NULL WHERE id = :id');
		$stmt->bindValue(':id', $user_id);
		$stmt->bindValue(':senha', password_hash($nova_senha, PASSWORD_DEFAULT));
		$stmt->execute();

    $_SESSION['success'] = 'Senha redefinida com sucesso!';
		header('Location: '.BASE_PATH.'/index.php');
		exit();
	}
}

require_once 'layout/header.php';
?>
<h2 class="page-title">Redefinir senha</h2>
<?php if (!$invalid): ?>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/reset/<?= $_GET['token'] ?>">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">Nova Senha</label>
    <div class="col-md-6">
      <input type="password" name="nova_senha" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Confirmar Senha</label>
    <div class="col-md-6">
      <input type="password" name="confirmar_senha" class="form-control">
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
        Salvar
    </button>
  </div>
</form>
<?php else: ?>
<p class="alert alert-danger" role="alert">
Este link já expirou. Você deve refazer o processo de redefinição de senha.
</p>
<script>
setTimeout(function() {
  window.location.href = '<?= BASE_PATH ?>/login/reset-password.php';
}, 3000);
</script>
<?php endif; ?>
</div>
<?php require_once 'layout/footer.php'; ?>