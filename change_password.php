<?php

require_once 'connection.php';

session_start();

if (!isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$senha_antiga = isset($_POST['senha_antiga']) ? trim($_POST['senha_antiga']) : null;
	$nova_senha = isset($_POST['nova_senha']) ? trim($_POST['nova_senha']) : null;

	$stmt = $conn->prepare('SELECT senha FROM usuarios WHERE id = :id');
	$stmt->bindValue(':id', $_SESSION['user_id']);
	$stmt->execute();
	$usuario = $stmt->fetch();

	if (empty($senha_antiga) || empty($nova_senha))
	{
		$errors[] = 'É obrigatório o preenchimento de ambos os campos.';
	}
	else if ( ! password_verify($senha_antiga, $usuario['senha']))
	{
		$errors[] = 'A senha antiga informada não confere.';
	}

	if (empty($errors))
	{
		$stmt = $conn->prepare('UPDATE usuarios SET senha = :senha WHERE id = :id');
		$stmt->bindValue(':id', $_SESSION['user_id']);
		$stmt->bindValue(':senha', password_hash($nova_senha, PASSWORD_DEFAULT));
		$stmt->execute();

		header('Location: '.BASE_PATH.'/index.php');
		exit();
	}
}

require_once 'layout/header.php';
?>
<h2 class="page-title">Alterar Senha</h2>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/change_password.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">Senha Antiga</label>
    <div class="col-md-6">
      <input type="password" name="senha_antiga" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Nova Senha</label>
    <div class="col-md-6">
      <input type="password" name="nova_senha" class="form-control">
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
        Salvar
    </button>
    <a href="<?= BASE_PATH ?>/" class="btn btn-link">Voltar</a>
  </div>
</form>
<?php require_once 'layout/footer.php'; ?>