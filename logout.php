<?php

require_once 'connection.php';

session_start();

if (!isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

$_SESSION = array();
session_destroy();

require_once 'layout/header.php';
?>
<div class="panel panel-default">
	<div class="panel-heading">Sessão encerrada!</div>
	<div class="panel-body">
		Você será redirecionado para a página inicial.
		<a href="<?= BASE_PATH ?>/">Página Inicial</a>
	</div>
</div>
<script>
setTimeout(function() {
	window.location.href = '<?= BASE_PATH ?>/';
}, 3000);
</script>
<?php require_once 'layout/footer.php'; ?>