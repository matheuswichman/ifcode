            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-md-6">
                        <a href="http://sapucaia.ifsul.edu.br" target="_blank">
                            <img src="<?= BASE_PATH; ?>/assets/images/logo-campus.png" style="margin-top: 6px;">
                        </a>
                    </div>
                    <div class="col-md-6 text-right">
                        <address class="endereco">
                        IFSul Câmpus Sapucaia do Sul<br>
                        Avenida Copacabana, 100, Bairro Piratini - Sapucaia do Sul/RS<br>
                        CEP 93.216-120 - Telefone (51) 3452-9214
                        </address>
                        <span class="assinatura">Desenvolvido por Matheus Wichman <!-- Eh Nois Que Voa! --></span>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?= BASE_PATH; ?>/assets/js/datatables.min.js"></script>
    <script src="<?= BASE_PATH; ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?= BASE_PATH; ?>/assets/js/jquery.mask.min.js"></script>
    <script src="<?= BASE_PATH; ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?= BASE_PATH; ?>/assets/js/select2.min.js"></script>
    <?php if (isset($scripts)): ?>
    <?php foreach($scripts as $script): ?>
    <script src="<?= BASE_PATH . $script; ?>"></script>
    <?php endforeach; ?>
	<?php endif; ?>

    <?php if(isset($_SESSION['success']) && !empty($_SESSION['success'])): ?>
    <script>
    swal("Tudo certo", "<?= $_SESSION['success']; ?>", "success");
    </script>
    <?php $_SESSION['success'] = null; ?>
    <?php endif; ?>
  </body>
</html>