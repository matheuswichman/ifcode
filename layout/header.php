<?php require_once dirname(__FILE__) . '/../config.php'; ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= BASE_PATH ?>/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= BASE_PATH ?>/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= BASE_PATH ?>/assets/images/favicon-16x16.png">
    <meta name="theme-color" content="#00a645">
    <title>IFCODE 2018</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="<?= BASE_PATH; ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= BASE_PATH; ?>/assets/css/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_PATH; ?>/assets/css/sweetalert.css"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_PATH; ?>/assets/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_PATH; ?>/assets/css/select2-bootstrap.min.css"/>
    <link href="<?= BASE_PATH; ?>/assets/css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=153483894810478";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="header">
            <a href="<?= BASE_PATH; ?>">
              <img class="logo" src="<?= BASE_PATH; ?>/assets/images/logo.png" alt="IFCODE2018">
            </a>
          </div>
          <?php include 'navigation.php' ?>
          <div class="content">
