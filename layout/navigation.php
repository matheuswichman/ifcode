<?php require_once dirname(__FILE__) . '/../config.php'; ?>
<ul class="nav nav-pills">
  <li class="visible-lg" role="presentation"><a href="<?= BASE_PATH ?>/">Início</a></li>
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
      Evento <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
      <li><a href="<?= BASE_PATH ?>/sobre">Sobre</a></li>
      <li><a href="<?= BASE_PATH ?>/organizadores">Organizadores</a></li>
      <li class="visible-md visible-sm visible-xs"><a href="https://goo.gl/maps/h9WD43jPdGR2" target="_blank">Como Chegar</a></li>
      <li class="visible-md visible-sm visible-xs"><a href="<?= BASE_PATH ?>/regulamento">Regulamento</a></li>
      <li class="visible-md visible-sm visible-xs"><a href="<?= BASE_PATH ?>/cronograma">Cronograma</a></li>
      <li><a href="<?= BASE_PATH ?>/parceiros">Parceiros</a></li>
      <?php if (!isset($_SESSION['subscription_id'])): ?>
      <li class="visible-md visible-sm visible-xs"><a href="<?= BASE_PATH ?>/subscription">Inscrição</a></li>
      <?php endif; ?>
      <li class="visible-md visible-sm visible-xs" role="presentation"><a href="<?= BASE_PATH ?>/contato">Contato</a></li>
    </ul>
  </li>
  <li class="visible-lg" role="presentation"><a href="<?= BASE_PATH ?>/regulamento">Regulamento</a></li>
  <li class="visible-lg" role="presentation"><a href="<?= BASE_PATH ?>/cronograma">Cronograma</a></li>
  <li class="visible-lg"><a href="https://goo.gl/maps/h9WD43jPdGR2" target="_blank">Como Chegar</a></li>
  <li class="visible-lg" role="presentation"><a href="<?= BASE_PATH ?>/contato">Contato</a></li>
  <?php if (!isset($_SESSION['subscription_id'])): ?>
  <li class="visible-lg"><a href="<?= BASE_PATH ?>/subscription">Inscrição</a></li>
  <?php endif; ?>
  <?php if (isset($_SESSION['user_id'])): ?>
    <li role="presentation"><a href="<?= BASE_PATH ?>/subscription/list.php">Inscrições</a></li>
    <li role="presentation" class="dropdown nav-right">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="glyphicon glyphicon-user"></i>
        <?= explode(' ', $_SESSION['nome'])[0]; ?> <span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        <?php if (isset($_SESSION['subscription_id'])): ?>
        <li><a href="<?= BASE_PATH ?>/subscription">Minha Inscrição</a></li>
        <?php endif; ?>
        <li><a href="<?= BASE_PATH ?>/change_password.php">Alterar Senha</a></li>
        <li><a href="<?= BASE_PATH ?>/edit_account.php">Editar Conta</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="<?= BASE_PATH ?>/logout.php">Encerrar Sessão</a></li>
      </ul>
    </li>
  <?php else: ?>
    <li role="presentation" class="nav-right"><a href="<?= BASE_PATH ?>/login">Entrar</a></li>
  <?php endif; ?>
</ul>