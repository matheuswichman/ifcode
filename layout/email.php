<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
</head>
<body style="padding: 20px 0; margin: 0; font-family: Helvetica, Arial, sans-serif; font-size: 14px; background-color: #f5f5f5;">
	<table cellspacing="0" cellpadding="0" align="center" style="width: 600px;">
		<tr>
			<td align="center" style="padding: 20px 0; border-bottom: 4px dashed #00a645;">
				<a href="<?= BASE_PATH ?>" target="_blank" style="font-size: 1.8em; font-weight: bold; text-decoration: none;">
					<img src="<?= BASE_PATH ?>/assets/images/logo-colorido.png" style="height: 100px;">
				</a>
			</td>
		</tr>
		<tr>
			<td bgcolor="#ffffff" style="padding: 15px; border-bottom: 4px dashed #00a645;">
				<?= $message ?>
			</td>
		</tr>
	</table>
</body>
</html>
