<?php

require_once '../connection.php';

require_once 'login_functions.php';

session_start();

if (isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$email = isset($_POST['email']) ? trim($_POST['email']) : null;
	$senha = isset($_POST['senha']) ? trim($_POST['senha']) : null;

	list($check, $data) = check_login($conn, $email, $senha);

	if ($check)
	{
		$_SESSION['user_id'] = $data['id'];
		$_SESSION['nome'] = $data['nome'];
		$_SESSION['email'] = $email;
		$_SESSION['admin'] = $data['admin'] != 0;

		header('Location: '.BASE_PATH.'/index.php');
		exit();
	}
	else
	{
		$errors = $data;
	}
}

include 'index.php';