<?php

function check_login($conn, $email = '', $senha = '')
{
	$errors = [];

	if (empty($email))
	{
		$errors[] = 'Você esqueceu de preencher o e-mail.';
	}
	elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$errors[] = 'O e-mail infomado não é válido.';
	}

	if (empty($senha))
	{
		$errors[] = 'Você esqueceu de preencher a senha.';
	}

	if (empty($errors))
	{
		$stmt = $conn->prepare("SELECT * FROM usuarios WHERE email = :email");
		$stmt->bindValue(':email', $email);
		$stmt->execute();
		$usuario = $stmt->fetch();

		if ($usuario)
		{
			if (password_verify($senha, $usuario['senha']))
			{
				return array(true, $usuario);
			}
			else
			{
				$errors[] = 'O endereço de email ou senha não foram encontrados.';
			}
		}
		else
		{
			$errors[] = 'O endereço de email ou senha não foram encontrados.';
		}
	}

	return array(false, $errors);
}

function get_subscription_id($conn, $user_id)
{
	$stmt = $conn->prepare("SELECT id FROM inscricoes WHERE usuario_id = :usuario AND edicao = :edicao");
	$stmt->bindValue(':usuario', $user_id);
	$stmt->bindValue(':edicao', EDICAO_ATUAL);
	$stmt->execute();
	$row = $stmt->fetch();
	return $row ? $row['id'] : false;
}

function send_reset_password_email($conn, $email)
{
	$errors = [];

	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$errors[] = 'O e-mail digitado não é válido';
	}

	if (empty($errors))
	{
		$stmt = $conn->prepare("SELECT * FROM usuarios WHERE email = :email");
		$stmt->bindValue(':email', $email);
		$stmt->execute();
		$usuario = $stmt->fetch();

		if ($usuario)
		{
			if (empty($usuario['token']) || link_has_expired($usuario['token_enviado_em']))
			{
				$id = $usuario['id'];
				$stmt = $conn->prepare("UPDATE usuarios SET
					token = :token,
					token_enviado_em = NOW()
					WHERE id = :id");
				$stmt->bindValue(':token', $token = hash('md5', 'REMORZITO' . $id));
				$stmt->bindValue(':id', $id);
				$stmt->execute();
			}
			else
			{
				$token = $usuario['token'];
			}

			$href = BASE_PATH . '/reset/' . $token;
			$message .= '<p>Olá,</p>';
			$message .= '<p>Clique neste <a href="'.$href.'">link</a> para redefinir sua senha.</p>';
			$message .= '<p>Caso o link acima não esteja disponível, copie e cole o endereço abaixo em seu navegador:</p>';
			$message .= '<p align="center"><code style="font-family: monospace;">'.$href.'</code></p>';
			$mail_sent = send_email($usuario['email'], 'Redefinição de Senha', $message);

			return array($mail_sent, null);
		}
		else
		{
			$errors[] = 'Ocorreu um erro ao redefinir a senha.';
		}
	}
	else
	{
		return array(false, $errors);
	}
}

function link_has_expired($link_sent_at)
{
	$now = strtotime('now');
	$diff = $now - strtotime($link_sent_at);
	$expiration_time = 60 * 60 * 24;
	return $diff > $expiration_time;
}