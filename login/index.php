<?php

session_start();

if (isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

require_once '../layout/header.php';
?>
<h2 class="page-title">Iniciar Sessão</h2>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/login/login.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">E-mail</label>
    <div class="col-md-6">
      <input type="text" name="email" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Senha</label>
    <div class="col-md-6">
      <input type="password" name="senha" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-6 reset-password">
      <a href="<?= BASE_PATH; ?>/login/reset-password.php">Esqueci a senha</a> |
      <a href="<?= BASE_PATH; ?>/register">Registre-se aqui</a> 
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
      Entrar
    </button>
  </div>
</form>
<?php require_once '../layout/footer.php'; ?>