<?php

require_once '../connection.php';
require_once '../global_functions.php';
require_once 'login_functions.php';

session_start();

if (isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $email = isset($_POST['email']) ? $_POST['email'] : null;

  list($check, $data) = send_reset_password_email($conn, $email);

  if ($check)
  {
    $_SESSION['success'] = 'Foi enviado um e-mail contendo o link para redefinição de senha.';
    header('Location: '.BASE_PATH.'/index.php');
    exit();
  }
  else
  {
    $errors = $data;
  }
}

require_once '../layout/header.php';
?>
<h2 class="page-title">Esqueci minha senha</h2>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/login/reset-password.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">E-mail</label>
    <div class="col-md-6">
      <input type="text" name="email" class="form-control" placeholder="Seu e-mail">
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
      Enviar
    </button>
  </div>
</form>
<?php require_once '../layout/footer.php'; ?>