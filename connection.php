<?php

require_once dirname(__FILE__) . '/config.php';

$conn = new PDO('mysql:host=localhost;dbname=banco;charset=utf8', 'usuario', 'senha');
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
