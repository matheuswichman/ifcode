<?php

error_reporting(E_ERROR);

$hoje = new DateTime();

define('MAX_INSCRICOES', 12);
define('EDICAO_ATUAL', 2018);
define('RECAPTCHA_KEY', 'foobar');
define('BASE_PATH', 'http://ifcode.vacaria.ifrs.edu.br');
define('CONTACT_EMAIL', 'usuario@exemplo.com');
define('INSCRICOES_ABERTAS', $hoje > (new DateTime('2018-08-08 00:00:00')) && $hoje < (new DateTime('2018-09-08 23:59:59')));
define('RESULTADOS_LIBERADOS', $hoje > (new DateTime('2018-09-15 00:00:00')));
define('MODIFICACOES_PERMITIDAS', INSCRICOES_ABERTAS || (RESULTADOS_LIBERADOS && $hoje < (new DateTime('2018-09-22 23:59:59'))));

define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USERNAME', 'usuario@exemplo.com');
define('SMTP_PASSWORD', 'senha');
define('SMTP_ENCRYPTION', 'ssl');
define('SMTP_PORT', 465);
define('MAIL_FROM', 'usuario@exemplo.com');

$situacoes = array(
  'P' => 'PENDENTE',
  'H' => 'HOMOLOGADA',
  'R' => 'REJEITADA',
);
