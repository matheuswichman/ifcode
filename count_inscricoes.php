<?php

include './connection.php';

$stmt = $conn->prepare('SELECT COUNT(*) AS total, modalidade FROM inscricoes GROUP BY modalidade');
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	echo $row['modalidade'] . ' = ' . $row['total'] . '<br>';
}