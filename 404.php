<?php
session_start();
require_once 'config.php';
require_once 'layout/header.php';
?>
<h2 class="page-title">Página não encontrada!</h2>
Você será redirecionado para a página inicial.
<a href="<?= BASE_PATH ?>/">Página Inicial</a>
<script>
setTimeout(function() {
	window.location.href = '<?= BASE_PATH ?>/';
}, 3000);
</script>
<?php require_once 'layout/footer.php'; ?>