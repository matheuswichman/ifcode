<?php

require_once '../connection.php';

require_once '../global_functions.php';

session_start();

$errors = [];

if (isset($_GET['assunto'])) {
	$assunto = $_GET['assunto'];
}

if (isset($_SESSION['user_id']))
{
  $stmt = $conn->prepare("SELECT nome, email FROM usuarios WHERE id = :id");
  $stmt->bindValue(':id', $_SESSION['user_id']);
  $stmt->execute();
  $usuario = $stmt->fetch();
  $nome = $usuario['nome'];
  $email = $usuario['email'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nome = isset($_POST['nome']) ? trim($_POST['nome']) : null;
	$email = isset($_POST['email']) ? trim($_POST['email']) : null;
	$assunto = isset($_POST['assunto']) ? trim($_POST['assunto']) : null;
	$mensagem = isset($_POST['mensagem']) ? trim($_POST['mensagem']) : null;

	$errors = array();

	if (empty($nome))
	{
		$errors[] = 'É necessário preencher o campo Nome.';
	}

	if (empty($email))
	{
		$errors[] = 'É necessário preencher o campo E-mail.';
	}
	elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$errors[] = 'O e-mail infomado não é válido.';
	}

	if (empty($assunto))
	{
		$errors[] = 'É necessário preencher o campo Assunto.';
	}

	if (empty($mensagem))
	{
		$errors[] = 'É necessário preencher o campo Mensagem.';
	}

	if (count($errors) == 0)
	{
		$message .= '<p><strong>Nome:</strong> '.$nome.'</p>';
		$message .= '<p><strong>E-mail:</strong> '.$email.'</p>';
		$message .= '<p><strong>Assunto:</strong> '.$assunto.'</p>';
		$message .= '<p><strong>Mensagem:</strong></p>';
		$message .= '<p>'.$mensagem.'</p>';
		$mail_sent = send_email(CONTACT_EMAIL, 'Novo Contato', $message, $email);

		if ($mail_sent)
		{
		    $_SESSION['success'] = 'Sua mensagem foi enviada com sucesso.';
		    header('Location: '.BASE_PATH.'/index.php');
		    exit();
		}
		else
		{
			$errors[] = 'Não foi possível enviar sua mensagem. Tente novamente.';
		}
	}
}

include 'contact.php';