<?php
require_once '../layout/header.php';
?>
<h2 class="page-title">Contato</h2>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/contact/index.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">Nome</label>
    <div class="col-md-6">
      <input type="text" name="nome" id="nome" class="form-control" value="<?= isset($nome) ? $nome : ''; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">E-mail</label>
    <div class="col-md-6">
      <input type="text" name="email" id="email" class="form-control" value="<?= isset($email) ? $email : '' ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Assunto</label>
    <div class="col-md-6">
      <input type="text" name="assunto" id="assunto" class="form-control" value="<?= isset($assunto) ? $assunto : '' ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Mensagem</label>
    <div class="col-md-6">
      <textarea name="mensagem" id="mensagem" class="form-control" rows="5"><?= isset($mensagem) ? $mensagem : '' ?></textarea>
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
      Enviar
    </button>
  </div>
</form>
<?php require_once '../layout/footer.php'; ?>