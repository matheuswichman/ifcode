<?php

require 'lib/class.phpmailer.php';
require 'lib/class.smtp.php';

function send_email($to, $subject, $message, $replyTo = '')
{
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->isHTML(true);
	$mail->SMTPDebug = 0;
	$mail->CharSet = 'UTF-8';
	$mail->Host = SMTP_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = SMTP_USERNAME;
	$mail->Password = SMTP_PASSWORD;
	$mail->SMTPSecure = SMTP_ENCRYPTION;
	$mail->Port = SMTP_PORT;
	$mail->setFrom(MAIL_FROM, 'IFCODE ' . EDICAO_ATUAL);
	$mail->addAddress($to);
	$mail->Subject = $subject;

	if ($replyTo)
	{
		$mail->addReplyTo($replyTo);
	}

	ob_start();
	include dirname(__FILE__) . '/layout/email.php';
	$html = ob_get_clean();
	$mail->Body = $html;

	return $mail->send();
}