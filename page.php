<?php

require_once 'config.php';

session_start();

$page = isset($_GET['page']) ? $_GET['page'] : '';
$filename = 'static/'.$page.'.php';

if (!file_exists($filename))
{
	header('Location: '.BASE_PATH.'/404.php');
	exit();
}

include $filename;

require_once 'layout/header.php';
?>
<div class="row">
	<div class="<?= $page == 'parceiros' ? 'col-md-12' : 'col-md-9' ?>">
		<h2 class="page-title"><?= $title; ?></h2>
		<?= $content ?>
	</div>
	<?php if ($page != 'parceiros'): ?>
	<div class="col-md-3 text-center sidebar">
		<?php include 'layout/sidebar.php'; ?>
	</div>
	<?php endif; ?>
</div>
<?php require_once 'layout/footer.php'; ?>