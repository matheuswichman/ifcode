$(function() {
	$('#member-modal').modal({
		keyboard: false,
		show: false
	});

	$('#instituicao').select2({
		placeholder: "Selecione uma instituição",
		theme: 'bootstrap'
	});

	$('.modalidade').click(function() {
		refreshMembers();
	});

	$('#subscription-form').submit(function(e) {
		e.preventDefault();
		saveSubscription();
		return false;
	});

	refreshMembers();

	$('#data_nascimento').mask('00/00/0000');

	if (!changesAllowed) {
		$('#subscription-form')
			.find('input,button,textarea,select')
			.prop('disabled', true);
	}
});

function saveMember()
{
	$('#save-member').prop('disabled', true);
	$('#errors').empty().hide();
	$('#member-form').find('.form-group').removeClass('has-error');
	$.post($('#member-form').attr('action'), $('#member-form').serialize(), function(response) {
		if (response.softwares == '1') {
			$('#softwares').text('');
			$('#softwares').closest('.form-group').show();
		} else{
			$('#softwares').text('');
			$('#softwares').closest('.form-group').hide();
		}
		$('#member-modal').modal('hide');
		$('#save-member').prop('disabled', false);
		refreshMembers();
	}).fail(function(jqXHR, textStatus, errorThrown) {
		$('#save-member').prop('disabled', false);
		$('#errors').empty().show();
		$.each(jqXHR.responseJSON, function(field, error) {
			$('#' + field).closest('.form-group').addClass('has-error');
			$('#errors').append('<p class="alert alert-danger alert-dismissible">' +
				'<button type="button" class="close" data-dismiss="alert">' +
				'<span aria-hidden="true">&times;</span>' +
				'</button>' + error + '</p>');
		});
	});
}

function refreshMembers()
{
	$.get('members.php?modalidade=' + $('.modalidade:checked').val(), function(response) {
		$('#members').html(response);

		if ($('.modalidade:checked').val() == 'M') {
			toggleSoftwaresInput(false);
		} else {
			var membrosUsandoComputadorDoEvento = $('#members li').filter(function() {
				return $(this).data('equipamento') == 1;
			}).size();
			toggleSoftwaresInput(membrosUsandoComputadorDoEvento > 0);
		}
	});
}

function addMember()
{
	var inscricao_id = $('#inscricao_id').val();
	var instituicao = $('#instituicao').val();
	var modalidade = $('.modalidade:checked').val();

	if (!instituicao.length) {
		alert('Escolha uma instituição primeiro.');
		return false;
	}

	$('#member-modal').modal('show');
	$('#modal-title').text('Novo Membro');
	$('#member-form').attr('action', 'add-member.php?id=' + inscricao_id + '&modalidade=' + modalidade + '&instituicao=' + instituicao);
	$('#member-form')[0].reset();
	$('#save-member').prop('disabled', false);

	if (!$('.members li').length)	{
		$('#nome').val(currentUser.nome).prop('readonly', true);
		$('#email').val(currentUser.email).prop('readonly', true);
	} else {
		$('#nome').prop('readonly', false);
		$('#email').prop('readonly', false);
	}

	if ($('.modalidade:checked').val() == 'M') {
		$('#equipamento').val('1').prop('disabled', true);
	} else {
		$('#equipamento').val('1').prop('disabled', false);
	}
}

function editMember(key, blockFields)
{
	var inscricao_id = $('#inscricao_id').val();
	var instituicao = $('#instituicao').val();
	var modalidade = $('.modalidade:checked').val();
	$('#member-modal').modal('show');
	$('#modal-title').text('Editar Membro');
	$('#member-form').attr('action', 'add-member.php?key=' + key + '&id=' + inscricao_id + '&modalidade=' + modalidade + '&instituicao=' + instituicao);
	$('#member-form')[0].reset();
	$('#save-member').prop('disabled', false);
	$.get('edit-member.php', {key: key}, function(response) {
		$('#nome').val(response.nome);
		$('#matricula').val(response.matricula);
		$('#curso').val(response.curso);
		$('#data_nascimento').val(response.data_nascimento);
		$('#email').val(response.email);
		$('#periodo').val(response.periodo);
		$('#apelido').val(response.apelido);
		$('#equipamento').val(response.equipamento);
		$('#camiseta-' + response.camiseta).prop('checked', true);
		$('#equipamento').prop('disabled', $('.modalidade:checked').val() == 'M');

		$('#nome').prop('readonly', blockFields);
		$('#email').prop('readonly', blockFields);
	});
}

function removeMember(key)
{
	if (!confirm('Tem certeza?'))
	{
		return false;
	}

	$.post('remove-member.php?key=' + key, function() {
		refreshMembers();
	});
}

function changeLeader(key)
{
	$.post('change-leader.php?key=' + key, function() {
		refreshMembers();
	});
}

function saveSubscription()
{
	var $form = $('#subscription-form');
	var $btn = $('#save-btn');
	$btn.prop('disabled', true);
	$.post($form.attr('action'), $form.serialize(), function(response) {
		swal({
			title: "Tudo certo", 
			text: "Inscrição realizada com sucesso.", 
			type: "success"
		}, function() {
			history.back();
		});
	}).fail(function(jqXHR, textStatus, errorThrown) {
		$('#errors_subscription').empty().show();
		$.each(jqXHR.responseJSON, function(field, error) {
			$('#' + field).closest('.form-group').addClass('has-error');
			$('#errors_subscription').append('<p class="alert alert-danger alert-dismissible">' +
				'<button type="button" class="close" data-dismiss="alert">' +
				'<span aria-hidden="true">&times;</span>' +
				'</button>' + error + '</p>');
		});
	}).always(function() {
		$btn.prop('disabled', false);
	});
}

function toggleSoftwaresInput(show)
{
	if (show)
	{
		// $('#softwares').text('');
		$('#softwares').closest('.form-group').show();
	}
	else
	{
		$('#softwares').text('');
		$('#softwares').closest('.form-group').hide();
	}
}