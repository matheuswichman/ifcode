var table;

$(function() {
	table = $('#subscriptions').DataTable({
		'processing': true,
		'serverSide': true,
		'ajax': '_grid.php',
		'language': {
			'url': '../assets/js/pt-br.json'
		},
		'columnDefs': [
			{
				'targets': [ -1 ],
				'searchable': false,
				'orderable': false
			}
		]
	});
});

function deleteSubscription(id) {
	if (confirm('Tem certeza?')) {
		$.post('delete-subscription.php?id=' + id, function() {
			table.ajax.reload();
			swal("Tudo certo", "Inscrição removida.", "success");
		}).fail(function() {
			alert('Erro ao remover inscrição');
		});
	}
}