<?php

session_start();

if (isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

require_once '../layout/header.php';
?>
<h2 class="page-title">Criar Nova Conta</h2>
<form class="form-horizontal" id="register-form" method="POST" action="<?= BASE_PATH; ?>/register/register.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">Nome</label>
    <div class="col-md-6">
      <input type="text" name="nome" class="form-control" value="<?= isset($nome) ? $nome : ''; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">E-mail</label>
    <div class="col-md-6">
      <input type="text" name="email" class="form-control" value="<?= isset($email) ? $email : ''; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Senha</label>
    <div class="col-md-6">
      <input type="password" name="senha" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label">Repetir Senha</label>
    <div class="col-md-6">
      <input type="password" name="repetir_senha" class="form-control">
    </div>
  </div>

  <div class="form-actions">
    <button class="g-recaptcha btn btn-primary" data-sitekey="6LccIioUAAAAACA7g60kweUazEnGtWn8-mVuqgCn" data-callback="onSubmit">
      Criar Conta
    </button>
  </div>
</form>
<script>
function onSubmit(token) {
 document.getElementById("register-form").submit();
}
</script>
<?php require_once '../layout/footer.php'; ?>