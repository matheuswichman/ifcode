<?php

require_once '../connection.php';

require_once 'register_functions.php';

session_start();

if (isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nome = isset($_POST['nome']) ? trim($_POST['nome']) : null;
	$email = isset($_POST['email']) ? trim($_POST['email']) : null;
	$senha = isset($_POST['senha']) ? trim($_POST['senha']) : null;
	$repetir_senha = isset($_POST['repetir_senha']) ? trim($_POST['repetir_senha']) : null;
	$recaptcha = $_POST['g-recaptcha-response'];

	list($check, $data) = register_user($conn, $nome, $email, $senha, $repetir_senha, $recaptcha);

	if ($check)
	{
		$_SESSION['user_id'] = $data;
		$_SESSION['nome'] = $nome;
		$_SESSION['email'] = $email;
		$_SESSION['admin'] = false;

		header('Location: '.BASE_PATH.'/index.php');
		exit();
	}
	else
	{
		$errors = $data;
	}
}

include 'index.php';