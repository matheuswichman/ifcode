<?php

function register_user($conn, $nome = '', $email = '', $senha = '', $repetir_senha = '', $recaptcha)
{
	$errors = [];

	if (empty($nome))
	{
		$errors[] = 'Você esqueceu de preencher o seu nome.';
	}

	if (empty($email))
	{
		$errors[] = 'Você esqueceu de preencher o e-mail.';
	}
	elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$errors[] = 'O e-mail infomado não é válido.';
	}

	if (empty($senha) || empty($repetir_senha))
	{
		$errors[] = 'Você esqueceu de preencher a senha.';
 	}
 	elseif ($senha != $repetir_senha)
 	{
	 	$errors[] = 'As senhas digitadas não conferem.';
 	}

	$data = http_build_query(array(
		'secret' => RECAPTCHA_KEY,
		'response' => $recaptcha,
		'remoteip' => $_SERVER['REMOTE_HOST']
	));

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$captcha = curl_exec($ch);
	curl_close($ch);

	$json = json_decode($captcha);
	if (!$json->success) {
		$errors[] = 'Ocorreu um erro ao validar o formulário.';
	}

	if (empty($errors))
	{
		$stmt = $conn->prepare("INSERT INTO usuarios (nome, email, senha) VALUES (:nome, :email, :senha)");
		$stmt->bindValue(':nome', $nome);
		$stmt->bindValue(':email', $email);
		$stmt->bindValue(':senha', password_hash($senha, PASSWORD_DEFAULT));
		try
		{
			$stmt->execute();
			return array(true, $conn->lastInsertId());
		}
		catch (Exception $e) {
			$errors[] = 'O endereço de e-mail digitado já está cadastrado.';
		}
	}

	return array(false, $errors);
}