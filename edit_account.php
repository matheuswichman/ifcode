<?php

require_once 'connection.php';

session_start();

if (!isset($_SESSION['user_id']))
{
  header('Location: '.BASE_PATH.'/index.php');
  exit();
}

$errors = [];
$nome = $_SESSION['nome'];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nome = isset($_POST['nome']) ? trim($_POST['nome']) : null;

	if (empty($nome))
	{
		$errors[] = 'É obrigatório preencher o campo Nome.';
	}

	if (empty($errors))
	{
    $stmt = $conn->prepare('UPDATE usuarios SET nome = :nome WHERE id = :id');
    $stmt->bindValue(':id', $_SESSION['user_id']);
    $stmt->bindValue(':nome', $nome);
    $stmt->execute();

		$stmt = $conn->prepare('UPDATE membros SET nome = :nome WHERE email = :email');
		$stmt->bindValue(':email', $_SESSION['email']);
		$stmt->bindValue(':nome', $nome);
		$stmt->execute();

    $_SESSION['nome'] = $nome;
    $_SESSION['success'] = 'Conta alterada com sucesso.';
		header('Location: '.BASE_PATH.'/index.php');
		exit();
	}
}

require_once 'layout/header.php';
?>
<h2 class="page-title">Editar Conta</h2>
<form class="form-horizontal" method="POST" action="<?= BASE_PATH; ?>/edit_account.php">
  <?php if (!empty($errors)): ?>
    <?php foreach($errors as $error): ?>
      <p class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <?= $error ?>
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="form-group">
    <label class="col-md-4 control-label">Nome</label>
    <div class="col-md-6">
      <input type="text" name="nome" class="form-control" value="<?= $nome; ?>">
    </div>
  </div>

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
        Salvar
    </button>
    <a href="<?= BASE_PATH ?>/" class="btn btn-link">Voltar</a>
  </div>
</form>
<?php require_once 'layout/footer.php'; ?>