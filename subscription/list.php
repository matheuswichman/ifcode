<?php

require_once '../connection.php';

require_once 'subscription_functions.php';

session_start();

if (!isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/login/');
	exit();
}

$scripts = [
	'/assets/js/subscriptions.js'
];

require_once '../layout/header.php';
?>
<h2 class="page-title">Inscrições</h2>
<table id="subscriptions" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
	    <th>ID</th>
	    <th>Equipe</th>
	    <th>Instituição</th>
	    <th>Modalidade</th>
	    <th>Data</th>
	    <th>Usuário</th>
	    <th>Situação</th>
	    <th><i class="glyphicon glyphicon-cog"></i></th>
		</tr>
	</thead>
</table>
<?php require_once '../layout/footer.php'; ?>