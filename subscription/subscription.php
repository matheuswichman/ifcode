<?php

require_once '../connection.php';
require_once '../global_functions.php';

require_once 'subscription_functions.php';

session_start();

if (!isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

if (!MODIFICACOES_PERMITIDAS && !$_SESSION['admin'])
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$equipe = isset($_POST['equipe']) ? trim($_POST['equipe']) : null;
	$instituicao = isset($_POST['instituicao']) ? trim($_POST['instituicao']) : null;
	$modalidade = isset($_POST['modalidade']) ? trim($_POST['modalidade']) : null;
	$softwares = isset($_POST['softwares']) ? trim($_POST['softwares']) : null;
	$situacao = isset($_POST['situacao']) ? trim($_POST['situacao']) : null;
	$motivo = isset($_POST['motivo']) ? trim($_POST['motivo']) : null;
	$confirmacao = isset($_POST['confirmacao']) ? trim($_POST['confirmacao']) : null;
	$softwares_resposta = isset($_POST['softwares_resposta']) ? trim($_POST['softwares_resposta']) : null;
	$members = $_SESSION['members'];

	if (empty($equipe))
	{
		$errors['equipe'] = 'Campo Nome não foi preenchido.';
	}

	if (empty($instituicao))
	{
		$errors['instituicao'] = 'Campo Instituição não foi preenchido.';
	}

	if (empty($confirmacao))
	{
		$errors['confirmacao'] = 'É necessário aceitar os termos do <a href="'.BASE_PATH.'/regulamento">regulamento</a>.';
	}

	if (empty($modalidade))
	{
		$errors['modalidade'] = 'Campo Modalidade não foi preenchido.';
	}
	elseif (!in_array($modalidade, ['H', 'M']))
	{
		$errors['modalidade'] = 'A modalidade informada não é válida.';
	}
	elseif (count($members) > ($members_num = members_total_by_category($modalidade)))
	{
		$errors['modalidade'] = "A modalidade escolhida só permite $members_num membros.";
	}
	elseif (count($members) != ($members_num = members_total_by_category($modalidade)))
	{
		$errors['members'] = "São necessários $members_num membros nesta equipe.";
	}

	if ($modalidade == 'H')
	{
		if (membros_por_periodo($members, '1º ano') == 0 && membros_por_periodo($members, '2º ano') == 0)
		{
			$errors['members'] = "No mínimo um dos membros da equipe deve ser aluno do 1º ou 2º ano do curso.";
		}

		if (membros_por_periodo($members, 'Egresso') > 2)
		{
			$errors['members'] = "No máximo dois dos membros da equipe podem ser egressos do curso.";
		}
	}

	if (count($errors) > 0) {
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode($errors);
		exit();
	}

	$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	$subscriptions_id = get_user_subscriptions_by_modalidade($conn, $modalidade, $_SESSION['user_id'], $id);
	if (!$_SESSION['admin'] && $subscriptions_id > 0) 
	{
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode(['nome' => 'Você já tem uma inscrição realizada nesta modalidade.']);
		exit();
	}

	if ($id > 0)
	{
		$subscription = get_subscription($conn, $id);

		if (!$_SESSION['admin'] && $subscription['usuario_id'] != $_SESSION['user_id'])
		{
			http_response_code(500);
			header('Content-Type: application/json');
			echo json_encode(['nome' => 'ID da inscrição inválido']);
			exit();
		}

		list($check, $data) = update_subscription(
			$conn, $equipe, $instituicao, $modalidade, $members, $id, $softwares
		);

		if ($_SESSION['admin'])
		{
			update_admin_subscription($conn, $id, $softwares_resposta, $situacao, $motivo);
		} else {
			$instituicao = get_instituicao($conn, $instituicao);
			$instituicao = $instituicao['nome'];
			$modalidade = $modalidade == 'H' ? 'Hackathon' : 'Maratona';

			$message = "<p><strong>Equipe:</strong> $equipe</p>";
			$message .= "<p><strong>Instituição:</strong>  $instituicao</p>";
			$message .= "<p><strong>Modalidade:</strong>  $modalidade</p>";
			$message .= "<p><strong>Participantes:</strong></p><hr>";
			foreach ($members as $member) {
				$message .= '<p>Nome / Matrícula: '.$member['nome'].' - '.$member['matricula'].'</p>';
				$message .= '<p>Curso / Data de Nascimento: '.$member['curso'].' - '.$member['data_nascimento'].'</p>';
				$message .= '<p>E-mail: '.$member['email'].'</p>';
				$message .= '<p>Período / Camiseta / Apelido: '.$member['periodo'].' - '.$member['camiseta'].' - '.$member['apelido'].'</p>';
				$message .= '<p>Equipamento: '.($member['equipamento'] == 1 ? 'Fornecido p/ evento' : 'Computador próprio').'</p>';
				$message .= '<p>Líder: '.($member['lider'] ? 'Sim' : 'Não').'</p><hr>';
			}
			send_email(CONTACT_EMAIL, 'Inscrição Editada - ID ' . $id, $message);
		}
	}
	else
	{
		list($check, $data) = create_subscription(
			$conn, $equipe, $instituicao, $modalidade, $members, $_SESSION['user_id'], $softwares
		);

		$instituicao = get_instituicao($conn, $instituicao);
		$instituicao = $instituicao['nome'];
		$modalidade = $modalidade == 'H' ? 'Hackathon' : 'Maratona';
		$nome_usuario = $_SESSION['nome'];

		$message = "<p><strong>Equipe:</strong> $equipe</p>";
		$message .= "<p><strong>Instituição:</strong>  $instituicao</p>";
		$message .= "<p><strong>Modalidade:</strong>  $modalidade</p>";
		$message .= "<p><strong>Criado Por:</strong> $nome_usuario</p>";
		$message .= "<p><strong>Participantes:</strong></p><hr>";
		foreach ($members as $member) {
			$message .= '<p>Nome / Matrícula: '.$member['nome'].' - '.$member['matricula'].'</p>';
			$message .= '<p>Curso / Data de Nascimento: '.$member['curso'].' - '.$member['data_nascimento'].'</p>';
			$message .= '<p>E-mail: '.$member['email'].'</p>';
			$message .= '<p>Período / Camiseta / Apelido: '.$member['periodo'].' - '.$member['camiseta'].' - '.$member['apelido'].'</p>';
			$message .= '<p>Equipamento: '.($member['equipamento'] == 1 ? 'Fornecido p/ evento' : 'Computador próprio').'</p>';
			$message .= '<p>Líder: '.($member['lider'] ? 'Sim' : 'Não').'</p><hr>';
		}
		send_email(CONTACT_EMAIL, 'Nova Inscrição - ID ' . $data, $message);
	}

	if ($check)
	{
		http_response_code(201);
		exit();
	}
	else
	{
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode(['nome' => $data]);
		exit();
	}
}