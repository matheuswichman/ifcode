<?php

require_once '../connection.php';
require_once 'subscription_functions.php';

session_start();

$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
$usuario_id = $_SESSION['user_id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($_SESSION['admin'])
	{	
		delete_subscription($conn, $id);
	}
	else
	{
		$ids = get_subscriptions_by_user($conn, $usuario_id);

		if (in_array($id, $ids))
		{
			delete_subscription($conn, $id);
		}
		else
		{
			http_response_code(500);
		}
	}
}