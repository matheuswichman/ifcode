<?php

require_once '../connection.php';

session_start();

if (!isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

$requestData = $_REQUEST;

$columns = [
	'inscricoes.id',
	'equipe',
	'instituicoes.sigla',
	'modalidade',
	'data',
	'usuarios.nome',
];

$stmt = $conn->prepare("SELECT COUNT(*) AS total FROM inscricoes");
$stmt->execute();
$row = $stmt->fetch();
$totalData = $row['total'];

$where = '';
if (!empty($requestData['search']['value'])) {
	$where .=" AND ( equipe LIKE '".$requestData['search']['value']."%' ";
	$where .=" OR usuarios.nome LIKE '".$requestData['search']['value']."%' )";
}

if (!$_SESSION['admin'])
{
	$where .= ' AND inscricoes.usuario_id = ' . $_SESSION['user_id'];
}

$stmt = $conn->prepare("SELECT COUNT(*) AS total FROM inscricoes
JOIN instituicoes ON inscricoes.instituicao_id = instituicoes.id
JOIN usuarios ON inscricoes.usuario_id = usuarios.id
WHERE 1 $where");
$stmt->execute();
$row = $stmt->fetch();
$totalFiltered = $row['total'];

$orderBy = " ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
$limit = " LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

$sql = "SELECT inscricoes.*, IFNULL(instituicoes.sigla, instituicoes.nome) AS instituicao, usuarios.nome AS usuario
FROM inscricoes
JOIN instituicoes ON inscricoes.instituicao_id = instituicoes.id
JOIN usuarios ON inscricoes.usuario_id = usuarios.id
WHERE 1 $where $orderBy $limit";
$stmt = $conn->prepare($sql);
$stmt->execute();

$data = array();
while ($subscription = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$id = $subscription['id'];
	$situacao = $subscription['situacao'];
	$data[] = array(
		$id,
		$subscription['equipe'],
		$subscription['instituicao'],
		$subscription['modalidade'] == 'H' ? 'Hackaton' : 'Maratona',
		date('d/m/Y H:i', strtotime($subscription['data'])),
		$subscription['usuario'],
		$situacoes[$situacao],
		'<a target="_blank" href="'.BASE_PATH.'/subscription?id='.$id.'" class="btn btn-success btn-xs" title="Editar"><span class="glyphicon glyphicon-edit"></span></a> ' .
		'<button type="button" class="btn btn-danger btn-xs" onclick="deleteSubscription('.$id.')" title="Remover"><span class="glyphicon glyphicon-trash"></span></button>',
	);
}

$json_data = array(
	'draw'            => intval($requestData['draw']),
	'recordsTotal'    => intval($totalData),
	'recordsFiltered' => intval($totalFiltered),
	'data'            => $data
);

echo json_encode($json_data);