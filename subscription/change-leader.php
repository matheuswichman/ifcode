<?php

session_start();

$newLeader = isset($_GET['key']) ? intval($_GET['key']) : 0;

if (!isset($_SESSION['members'][$newLeader]))
{
	http_response_code(500);
	exit();
}

foreach (array_keys($_SESSION['members']) as $key) {
	$_SESSION['members'][$key]['lider'] = ($key == $newLeader);
}