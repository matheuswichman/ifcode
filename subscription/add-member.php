<?php

require_once '../connection.php';
require_once 'subscription_functions.php';

session_start();

if (!isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/index.php');
	exit();
}

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nome = isset($_POST['nome']) ? trim($_POST['nome']) : null;
	$matricula = isset($_POST['matricula']) ? trim($_POST['matricula']) : null;
	$curso = isset($_POST['curso']) ? trim($_POST['curso']) : null;
	$data_nascimento = isset($_POST['data_nascimento']) ? trim($_POST['data_nascimento']) : null;
	$email = isset($_POST['email']) ? trim($_POST['email']) : null;
	$periodo = isset($_POST['periodo']) ? trim($_POST['periodo']) : null;
	$apelido = isset($_POST['apelido']) ? trim($_POST['apelido']) : null;
	$camiseta = isset($_POST['camiseta']) ? trim($_POST['camiseta']) : null;
	$equipamento = isset($_POST['equipamento']) ? intval($_POST['equipamento']) : null;
	$id = isset($_GET['id']) ? intval($_GET['id']) : -1;
	$modalidade = isset($_GET['modalidade']) ? $_GET['modalidade'] : null;
	$instituicao = isset($_GET['instituicao']) ? $_GET['instituicao'] : null;

	if ($modalidade == 'M')
	{
		$equipamento = 1;
	}

	if ($id < 0 || !$modalidade) 
	{	
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode(['matricula' => 'ID da inscrição ou modalidade não informados']);
		exit();
	}

	if (empty($nome))
	{
		$errors['nome'] = 'Campo Nome não foi preenchido.';
	}

	if (empty($periodo))
	{
		$errors['periodo'] = 'Campo Período não foi preenchido.';
	}
	elseif($periodo != 'Egresso')
	{
		if (empty($matricula))
		{
			$errors['matricula'] = 'Campo Matrícula não foi preenchido.';
		}
		elseif ($instituicao == 1 && !preg_match('/([0-9]{6}[A-Z]{4})/', $matricula))
		{
			$errors['matricula'] = 'A matrícula informada não é válida. Exemplo: 042970INFG';
		}
		elseif (member_already_registred($conn, $matricula, $modalidade, $id))
		{
			$errors['matricula'] = 'Esta matrícula já foi inserida em outra equipe da mesma modalidade.';
		}
	}

	if (empty($curso))
	{
		$errors['curso'] = 'Campo Curso não foi preenchido.';
	}

	if (empty($data_nascimento))
	{
		$errors['data_nascimento'] = 'Campo Data de Nascimento não foi preenchido.';
	}
	elseif (!preg_match('/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/', $data_nascimento))
	{
		$errors['data_nascimento'] = 'A data de nascimento informada não é válida.';
	}

	if (empty($email))
	{
		$errors['email'] = 'Campo E-mail não foi preenchido.';
	}
	elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$errors['email'] = 'O e-mail informado não é válida.';
	}

	if (empty($camiseta))
	{
		$errors['camiseta'] = 'Campo Camiseta não foi preenchido.';
	}

	if (empty($equipamento))
	{
		$errors['equipamento'] = 'Campo Equipamento não foi preenchido.';
	}

	$exists = false;
	$editingKey = isset($_GET['key']) ? intval($_GET['key']) : -1;
	foreach ($_SESSION['members'] as $key => $member) {
		if ($key == $editingKey)
			continue;
		if ($member['email'] == $email || (!empty($matricula) && $member['matricula'] == $matricula))
			$exists = true;
	}

	if ($exists)
	{
		$errors['email'] = 'O e-mail e/ou matrícula já estão cadastrados nesta equipe.';
	}

	if (count($errors) > 0) {
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode($errors);
		exit();
	}

	$data = [
		'nome'            => $nome,
		'matricula'       => $matricula,
		'curso'           => $curso,
		'data_nascimento' => $data_nascimento,
		'email'           => $email,
		'periodo'         => $periodo,
		'apelido'         => $apelido,
		'camiseta'        => $camiseta,
		'equipamento'     => $equipamento,
		'lider'           => count($_SESSION['members']) == 0,
	];

	if ($editingKey >= 0) {
		$data['lider'] = $_SESSION['members'][$editingKey]['lider'];
		$_SESSION['members'][$editingKey] = $data;
	}
	else
	{
		$_SESSION['members'][] = $data;
	}

	$computador_proprio = false;
	foreach ($_SESSION['members'] as $member) {
		$computador_proprio = $member['equipamento'] == 2;
		if ($computador_proprio)
			break;
	}

	http_response_code(200);
	header('Content-Type: application/json');
	echo json_encode(['softwares' => (!$computador_proprio ? '1' : '0')]);
	exit();
}