<?php

function set_new_leader(&$members, $key = 0)
{
	if (count($members) > 0)
	{
		$members[$key]['lider'] = false;
		$key = 0;
		while($key < count($members) && isset($members[$key]['removido']))
		{
			$key++;
		}
		$members[$key]['lider'] = true;
	}
}

function members_total_by_category($category)
{
	switch ($category) {
		case 'H':
			return 5;
			break;
		case 'M':
			return 3;
			break;
		default:
			return 0;
			break;
	}
}

function can_add_member($members, $category)
{
	$members_total = count($members);
	return members_total_by_category($category) > $members_total;
}

function create_subscription($conn, $equipe, $instituicao, $modalidade, $membros, $usuario_id, $softwares)
{
	$conn->beginTransaction();
	$stmt = $conn->prepare("
		INSERT INTO inscricoes (equipe, instituicao_id, modalidade, usuario_id, edicao, softwares)
		VALUES (:equipe, :instituicao, :modalidade, :usuario, :edicao, :softwares)");
	$stmt->bindValue(':equipe', $equipe);
	$stmt->bindValue(':instituicao', $instituicao);
	$stmt->bindValue(':modalidade', $modalidade);
	$stmt->bindValue(':usuario', $usuario_id);
	$stmt->bindValue(':softwares', $softwares);
	$stmt->bindValue(':edicao', EDICAO_ATUAL);
	try {
		$stmt->execute();
	}
	catch (Exception $e) {
		$conn->rollBack();
		return array(false, 'Este usuário já está inscrito nesta edição do evento.');
	}
	$inscricao_id = $conn->lastInsertId();
	foreach ($membros as $membro)
	{
		create_member($conn, $inscricao_id, $membro);
	}
	try {
		$conn->commit();
	}
	catch (Exception $e) {
		$conn->rollBack();
		return array(false, 'Ocorreu um erro ao salvar inscrição.');
	}
	return array(true, $inscricao_id);
}

function update_subscription($conn, $equipe, $instituicao, $modalidade, $membros, $id, $softwares)
{
	$conn->beginTransaction();
	$stmt = $conn->prepare("UPDATE inscricoes SET
		equipe = :equipe,
		instituicao_id = :instituicao,
		modalidade = :modalidade,
		softwares = :softwares
		WHERE id = :id");
	$stmt->bindValue(':equipe', $equipe);
	$stmt->bindValue(':instituicao', $instituicao);
	$stmt->bindValue(':modalidade', $modalidade);
	$stmt->bindValue(':softwares', $softwares);
	$stmt->bindValue(':id', $id);
	try {
		$stmt->execute();
	}
	catch (Exception $e) {
		$conn->rollBack();
		return array(false, 'Este usuário já está inscrito nesta edição do evento.');
	}

	$stmt = $conn->prepare("DELETE FROM membros WHERE inscricao_id = :id");
	$stmt->bindValue(':id', $id);
	$stmt->execute();

	foreach ($membros as $membro)
	{
		create_member($conn, $id, $membro);
	}
	try {
		$conn->commit();
	}
	catch (Exception $e) {
		$conn->rollBack();
		return array(false, 'Ocorreu um erro ao salvar inscrição.');
	}
	return array(true, $id);
}

function create_member($conn, $inscricao_id, $membro)
{
	$data_nascimento = DateTime::createFromFormat('d/m/Y', $membro['data_nascimento']);
	$stmt = $conn->prepare("
		INSERT INTO membros (inscricao_id, nome, matricula, curso, data_nascimento, email, periodo, apelido, camiseta, lider, equipamento)
		VALUES (:inscricao, :nome, :matricula, :curso, :data_nascimento, :email, :periodo, :apelido, :camiseta, :lider, :equipamento)");
	$stmt->bindValue(':inscricao', $inscricao_id);
	$stmt->bindValue(':nome', $membro['nome']);
	$stmt->bindValue(':matricula', $membro['matricula']);
	$stmt->bindValue(':curso', $membro['curso']);
	$stmt->bindValue(':data_nascimento', $data_nascimento->format('Y-m-d'));
	$stmt->bindValue(':email', $membro['email']);
	$stmt->bindValue(':periodo', $membro['periodo']);
	$stmt->bindValue(':apelido', $membro['apelido']);
	$stmt->bindValue(':camiseta', $membro['camiseta']);
	$stmt->bindValue(':equipamento', $membro['equipamento']);
	$stmt->bindValue(':lider', $membro['lider'] ? 1 : 0);
	$stmt->execute();
}

function update_admin_subscription($conn, $id, $softwares_resposta, $situacao, $motivo)
{
	$stmt = $conn->prepare("UPDATE inscricoes SET
		softwares_resposta = :softwares_resposta,
		situacao = :situacao,
		motivo = :motivo
		WHERE id = :id");
	$stmt->bindValue(':softwares_resposta', $softwares_resposta);
	$stmt->bindValue(':situacao', $situacao);
	$stmt->bindValue(':motivo', $motivo);
	$stmt->bindValue(':id', $id);
	$stmt->execute();
}

function get_subscription($conn, $id)
{
	$stmt = $conn->prepare("SELECT * FROM inscricoes WHERE id = :id");
	$stmt->bindValue(':id', $id);
	$stmt->execute();
	return $stmt->fetch();
}

function get_instituicao($conn, $id)
{
	$stmt = $conn->prepare("SELECT * FROM instituicoes WHERE id = :id");
	$stmt->bindValue(':id', $id);
	$stmt->execute();
	return $stmt->fetch();
}

function get_members($conn, $id)
{
	$stmt = $conn->prepare("SELECT * FROM membros WHERE inscricao_id = :id");
	$stmt->bindValue(':id', $id);
	$stmt->execute();
	$members = array();
	while ($member = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$member['data_nascimento'] = strftime('%d/%m/%Y', strtotime($member['data_nascimento']));
		$members[] = $member;
	}
	return $members;
}

function membros_por_periodo($membros, $periodo)
{
	$count = 0;
	foreach ($membros as $membro) {
		if ($membro['periodo'] == $periodo)
			$count++;
	}
	return $count;
}

function get_total_subscriptions($conn)
{
	$stmt = $conn->prepare("SELECT COUNT(id) AS total FROM inscricoes");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	return intval($row['total']);
}

function member_already_registred($conn, $matricula, $modalidade, $id)
{
	$sql = "SELECT COUNT(m.id) AS total FROM membros m
	JOIN inscricoes i ON m.inscricao_id = i.id
	WHERE m.matricula = :matricula AND i.modalidade = :modalidade AND i.id != :id";
	$stmt = $conn->prepare($sql);
	$stmt->bindValue(':id', $id);
	$stmt->bindValue(':matricula', $matricula);
	$stmt->bindValue(':modalidade', $modalidade);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	return intval($row['total']) > 0;
}

function get_user_subscriptions_by_modalidade($conn, $modalidade, $usuario_id, $id)
{
	$sql = "SELECT COUNT(id) AS total FROM inscricoes
	WHERE modalidade = :modalidade
	AND usuario_id = :usuario_id
	AND id != :id";
	$stmt = $conn->prepare($sql);
	$stmt->bindValue(':id', $id);
	$stmt->bindValue(':modalidade', $modalidade);
	$stmt->bindValue(':usuario_id', $usuario_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	return intval($row['total']) > 0;
}

function get_subscriptions_by_user($conn, $usuario_id)
{
	$sql = "SELECT id FROM inscricoes
	WHERE usuario_id = :usuario_id";
	$stmt = $conn->prepare($sql);
	$stmt->bindValue(':usuario_id', $usuario_id);
	$stmt->execute();
	$ids = [];
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$ids[] = $row['id'];
	}
	return $ids;
}

function delete_subscription($conn, $id)
{
	$stmt = $conn->prepare('DELETE FROM inscricoes WHERE id = :id');
	$stmt->bindValue(':id', $id);
	$stmt->execute();
}

function get_instituicoes($conn)
{
	$stmt = $conn->prepare("SELECT * FROM instituicoes ORDER BY nome");
	$stmt->execute();
	$instituicoes = [];
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$instituicoes[] = $row;
	}
	return $instituicoes;
}