<?php

session_start();

$key = isset($_GET['key']) ? intval($_GET['key']) : 0;

if (!isset($_SESSION['members'][$key]))
{
	http_response_code(500);
	exit();
}

header('Content-Type: application/json');
echo json_encode($_SESSION['members'][$key]);