<div class="modal fade" id="member-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-title">Novo Membro</h4>
      </div>
      <div class="modal-body">
        <div id="errors"></div>
        <form id="member-form">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label">Nome</label>
                <input type="text" name="nome" id="nome" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Matrícula</label>
                <input type="text" name="matricula" id="matricula" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label">Curso</label>
                <input type="text" name="curso" id="curso" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Data de Nascimento</label>
                <input type="text" name="data_nascimento" id="data_nascimento" class="form-control" placeholder="DD/MM/AAAA">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label">E-mail</label>
                <input type="text" name="email" id="email" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Equipamento</label>
                <select name="equipamento" id="equipamento" class="form-control">
                  <option value="1">Fornecido p/ evento</option>
                  <option value="2">Computador próprio</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Período</label>
                <select name="periodo" id="periodo" class="form-control">
                  <option value="">- Selecione -</option>
                  <option value="1º ano">1º ano (1º ou 2º semestre)</option>
                  <option value="2º ano">2º ano (3º ou 4º semestre)</option>
                  <option value="3º ano">3º ano (5º ou 6º semestre)</option>
                  <option value="4º ano">4º ano (7º ou 8º semestre)</option>
                  <option value="Egresso">Egresso</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Apelido</label>
                <input type="text" name="apelido" id="apelido" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Camiseta</label>
                <div id="camiseta">
                  <label class="radio-inline">
                    <input type="radio" name="camiseta" id="camiseta-P" value="P"> P
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="camiseta" id="camiseta-M" value="M"> M
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="camiseta" id="camiseta-G" value="G"> G
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <span class="help-block">
                <strong>Período:</strong> Deve-se informar o ano-turma em que o aluno tem maior carga horária.
              </span>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="saveMember()" id="save-member">Adicionar</button>
      </div>
    </div>
  </div>
</div>