<?php

require_once 'subscription_functions.php';

session_start();

$key = isset($_GET['key']) ? intval($_GET['key']) : 0;

$members = $_SESSION['members'];

if (!isset($members[$key]))
{
	http_response_code(500);
	exit();
}

if (isset($members[$key]['id']))
{
	$members[$key]['removido'] = true;

	if ($members[$key]['lider'])
	{
		set_new_leader($members, $key);
	}

	$_SESSION['members'] = $members;
}
else
{
	$member = $members[$key];
	unset($members[$key]);
	$members = array_values($members);

	if ($member['lider'])
	{
		set_new_leader($members);
	}

	$_SESSION['members'] = $members;
}