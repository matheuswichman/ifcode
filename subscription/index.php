<?php

require_once '../connection.php';
require_once 'subscription_functions.php';

session_start();

if (!isset($_SESSION['user_id']))
{
	header('Location: '.BASE_PATH.'/login');
	exit();
}

$id = isset($_GET['id']) ? intval($_GET['id']) : 0;

if (!INSCRICOES_ABERTAS && !$_SESSION['admin'] && $id == 0) {
  header('Location: '.BASE_PATH.'/');
  exit();
}

if ($id > 0) {
  $subscription = get_subscription($conn, $id);
  $_SESSION['members'] = get_members($conn, $id);

  if (!$_SESSION['admin'] && $subscription['usuario_id'] != $_SESSION['user_id'])
  {
    header('Location: '.BASE_PATH.'/');
    exit();
  }

  $usa_computador_do_evento = false;
  foreach ($_SESSION['members'] as $member) {
    if ($member['equipamento'] == 1) {
      $usa_computador_do_evento = true;
      break;
    }
  }
  if ($subscription['modalidade'] == 'M') 
    $usa_computador_do_evento = false;
} else {
  $usa_computador_do_evento = false;
  $_SESSION['members'] = [];
}

$errorMessage = null;

$scripts = [
  '/assets/js/subscription.js'
];

require_once '../layout/header.php';
?>
<h2 class="page-title"><?= $id > 0 ? 'Editar Inscrição' : 'Inscreva-se' ?></h2>
<form class="form-horizontal" id="subscription-form" method="POST" action="<?= BASE_PATH; ?>/subscription/subscription.php?id=<?= $id ?>">
  <input type="hidden" name="inscricao_id" id="inscricao_id" value="<?= $id ?>">

  <div id="errors_subscription"<?= empty($errorMessage) ? ' style="display: none;"' : ''; ?>>
    <?php if (!empty($errorMessage)): ?>
    <p class="alert alert-danger"><?= $errorMessage; ?></p>
    <?php endif; ?>    
  </div>

  <p class="alert alert-warning alert-dismissible">
    Para editar suas inscrições <a href="<?= BASE_PATH ?>/subscription/list.php">acesse aqui</a>.
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">&times;</span>
    </button>
  </p>

  <?php if (isset($subscription)): ?>
  <div class="form-group">
    <label class="control-label">Situação</label>
    <div class="col-md-6">
      <div class="alert alert-dark">
        <div class="situacao">
          <?php if ($_SESSION['admin']): ?>
          <select name="situacao" class="form-control">
            <?php foreach($situacoes as $key => $value): ?>
            <option value="<?= $key; ?>"<?= isset($subscription) && $subscription['situacao'] == $key ? ' selected' : '' ?>><?= $value; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <strong><?= RESULTADOS_LIBERADOS ? $situacoes[$subscription['situacao']] : 'PENDENTE'; ?></strong>
          <?php endif; ?>
        </div>
        <strong>MOTIVO:</strong><br>
        <?php if ($_SESSION['admin']): ?>
        <textarea name="motivo" id="motivo" rows="5" class="form-control"><?= isset($subscription) ? $subscription['motivo'] : ''; ?></textarea>
        <?php else: ?>
        <p><?= !empty($subscription['motivo']) && RESULTADOS_LIBERADOS ? $subscription['motivo'] : '- N/A -'; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="form-group">
    <label class="control-label">Nome da Equipe</label>
    <div class="col-md-6">
      <input type="text" name="equipe" id="equipe" class="form-control" value="<?= isset($subscription) ? $subscription['equipe'] : ''; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label">Instituição</label>
    <div class="col-md-6">
      <select class="form-control" id="instituicao" name="instituicao">
        <option></option>
        <?php
        $instituicoes = get_instituicoes($conn);
        foreach($instituicoes as $instituicao) {
          $selected = isset($subscription) && $subscription['instituicao_id'] == $instituicao['id'] ? ' selected' : '';
          echo '<option value="'.$instituicao['id'].'"'.$selected.'>'.$instituicao['nome'].'</option>';
        };
        ?>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label" id="modalidade">Modalidade</label>
    <div class="col-md-6">
			<label class="radio-inline">
			  <input type="radio" name="modalidade" class="modalidade" value="H"<?= !isset($subscription) || $subscription['modalidade'] == 'H' ? ' checked' : ''; ?>> Hackathon
			</label>
			<label class="radio-inline">
			  <input type="radio" name="modalidade" class="modalidade" value="M"<?= isset($subscription) && $subscription['modalidade'] == 'M' ? ' checked' : ''; ?>> Maratona
			</label>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label">Membros</label>
    <div class="col-md-6">
      <div id="members"></div>
    </div>
  </div>
  
  <div class="form-group"<?= ! $usa_computador_do_evento ? ' style="display: none;"' : ''; ?>>
    <label class="control-label">Softwares Adicionais</label>
    <div class="col-md-6">
      <textarea name="softwares" id="softwares" rows="5" class="form-control"><?= isset($subscription) ? $subscription['softwares'] : ''; ?></textarea>
      <span class="help-block">Informe os softwares e/ou tecnologias adicionais que deverão estar disponíveis para a equipe. Veja <a href="<?= BASE_PATH ?>/configuracao" target="_blank">aqui</a> a configuração dos computadores fornecidos pelo evento.</span>
      <?php if (isset($subscription) && !empty($subscription['softwares_resposta']) || $_SESSION['admin']): ?>
      <p class="alert alert-dark resposta">
        <strong>RESPOSTA:</strong><br>
        <?php if ($_SESSION['admin']): ?>
          <textarea name="softwares_resposta" id="softwares_resposta" rows="5" class="form-control"><?= isset($subscription) ? $subscription['softwares_resposta'] : ''; ?></textarea>
        <?php else: ?>
          <?= RESULTADOS_LIBERADOS ? $subscription['softwares_resposta'] : '- N/A -'; ?>
        <?php endif; ?>
      </p>
      <?php endif; ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-6 confirmacao">
      <label><input type="checkbox" name="confirmacao" id="confirmacao" value="1"<?= $_SESSION['admin'] ? ' checked' : ''; ?>> Li e aceito os termos do <a href="<?= BASE_PATH ?>/regulamento" target="_blank">regulamento</a> do evento.</label>
    </div>
  </div>

	<div class="form-actions">
    <button type="button" id="save-btn" class="btn btn-primary" onclick="saveSubscription()">
        Salvar
    </button>
  </div>
</form>
<script>
var changesAllowed = <?= MODIFICACOES_PERMITIDAS || $_SESSION['admin'] ? 'true' : 'false'; ?>;
var currentUser = <?= json_encode(['nome' => $_SESSION['nome'], 'email' => $_SESSION['email']]); ?>
</script>
<?php include '_modal.php'; ?>
<?php require_once '../layout/footer.php'; ?>