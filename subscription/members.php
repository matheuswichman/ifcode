<?php
require_once 'subscription_functions.php';
require_once '../config.php';

session_start();

$modalidade = $_GET['modalidade'];

if ($modalidade == 'M')
{
	foreach($_SESSION['members'] as $key => $member)
	{
		$_SESSION['members'][$key]['equipamento'] = '1';
	}
}
?>
<?php if (isset($_SESSION['members']) && count($_SESSION['members']) > 0): ?>
<ul class="members">
	<?php foreach($_SESSION['members'] as $key => $member): ?>
	<li data-equipamento="<?= $member['equipamento'] ?>">
		<span class="member-name"><?= $member['nome']; ?> - <?= $member['periodo']; ?></span>
		<?php if ($member['lider']): ?>
		<span class="badge" title="Líder da Equipe"><i class="glyphicon glyphicon-bookmark"></i></span>
		<?php endif; ?>
		<span class="pull-right">
			<?php if (!$member['lider']): ?>
			<button type="button" class="btn btn-xs btn-warning" title="Tornar Líder" onclick="changeLeader(<?= $key; ?>)"><i class="glyphicon glyphicon-bookmark"></i></button>
			<?php endif; ?>
			<?php if (MODIFICACOES_PERMITIDAS || $_SESSION['admin']): ?>
			<button type="button" class="btn btn-xs btn-success" title="Editar Membro" onclick="editMember(<?= $key; ?>, <?= $member['email'] == $_SESSION['email'] && !$_SESSION['admin'] ? 'true' : 'false' ?>)"><i class="glyphicon glyphicon-edit"></i></button>
			<button type="button" class="btn btn-xs btn-danger" title="Remover Membro" onclick="removeMember(<?= $key; ?>)"><i class="glyphicon glyphicon-trash"></i></button>
			<?php endif; ?>
		</span>
	</li>
	<?php endforeach; ?>
</ul>
<span class="help-block">Membro com <i class="glyphicon glyphicon-bookmark"></i> indica o líder da equipe.</span>
<button type="button" class="btn btn-xs btn-primary pull-right" <?= can_add_member($_SESSION['members'], $_GET['modalidade']) ? 'onclick="addMember()"' : 'disabled' ?>>Novo Membro</button>
<?php else: ?>
<p class="alert alert-success"><i class="glyphicon glyphicon-star"></i> Você deve ser um dos membros da equipe.</p>
<button type="button" class="add-first-member" onclick="addMember()">Adicione o primeiro</button>
<?php endif; ?>
