<?php

$title = 'Sobre';

$content = '<p>';
$content .= 'O IF Code é um evento que busca incentivar e proporcionar aos alunos da Instituição o desenvolvimento de habilidades de programação e criatividade. ';
$content .= 'O evento se divide em duas modalidades: <strong>Hackathon</strong> e <strong>Maratona:</strong></p>';
$content .= '<h3 class="titulo">Hackathon</h3>';
$content .= '<p>Hackathon é uma atividade em que as equipes participam de uma maratona de programação em regime intensivo, combinando habilidades de programação, prototipagem, criatividade e colaboração para, em um curto espaço de tempo, promover o desenvolvimento de soluções tecnológicas para um dado problema em uma temática disponibilizada pela organização do Evento.</p>';
$content .= '<h3 class="titulo">Maratona</h3>';
$content .= '<p>Na Maratona, as equipes de três participantes resolvem problemas desafiadores de programação em diferentes níveis de dificuldade e quem resolver o maior número sai vencedor. Esta modalidade promove nos estudantes a criatividade, a capacidade de trabalho em equipe, a busca de novas soluções de software e a habilidade de resolver problemas sob pressão.</p>';