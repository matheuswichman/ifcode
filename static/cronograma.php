<?php

$title = 'Cronograma';

$content = '<table class="table table-bordered table-striped">';
$content .= '<tr><th colspan="2">Datas e Prazos para Inscrição</th></tr>';
$content .= '<tr><td>Abertura das inscrições</td><td class="text-center" style="width: 150px;">08/08/2018 - 00:00</td></tr>';
$content .= '<tr><td>Encerramento das inscrições</td><td class="text-center">08/09/2018 - 23:59</td></tr>';
$content .= '<tr><td>Divulgação das inscrições homologadas</td><td class="text-center">15/09/2018 - 00:00</td></tr>';
$content .= '<tr><td>Atualização das inscrições não homologadas</td><td class="text-center">18/09/2018 - 23:59</td></tr>';
$content .= '<tr><td>Divulgação final das inscrições Homologadas</td><td class="text-center">20/09/2018 - 00:00</td></tr>';
$content .= '</table><br>';

$content .= '<table class="table table-bordered table-striped">';
$content .= '<tr><th colspan="2">Cronograma do Evento</th></tr>';
$content .= '<tr><td>Abertura do evento</td><td class="text-center" style="width: 150px;">29/09/2018 - 08:00</td></tr>';
$content .= '<tr><td>Início da Maratona</td><td class="text-center">29/09/2018 - 08:30</td></tr>';
$content .= '<tr><td>Término da Maratona</td><td class="text-center">29/09/2018 - 12:30</td></tr>';
$content .= '<tr><td>Início da Hackathon</td><td class="text-center">29/09/2018 - 14:30</td></tr>';
$content .= '<tr><td>Término da Hackathon</td><td class="text-center">30/09/2018 - 14:30</td></tr>';
$content .= '<tr><td>Início das bancas técnicas da Hackathon</td><td class="text-center">30/09/2018 - 14:30</td></tr>';
$content .= '<tr><td>Início da apresentação dos projetos da Hackathon</td><td class="text-center">30/09/2018 - 15:30</td></tr>';
$content .= '<tr><td>Início da cerimônia de premiação</td><td class="text-center">30/09/2018 - 18:00</td></tr>';
$content .= '</table>';
