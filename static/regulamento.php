<?php

$title = 'Regulamento';

$content = '<p>Os regulamentos estão disponíveis abaixo, separados por modalidade:</p>';
$content .= '<h3 class="titulo">Hackathon</h3>';
$content .= '<a href="'.BASE_PATH.'/assets/regulamento-hackathon.pdf" target="_blank">Baixe aqui</a>';
$content .= '<h3 class="titulo">Maratona</h3>';
$content .= '<a href="'.BASE_PATH.'/assets/regulamento-maratona.pdf" target="_blank">Baixe aqui</a>';
$content .= '<h3 class="titulo">Autorização para Menores de Idade</h3>';
$content .= '<a href="'.BASE_PATH.'/assets/autorizacao.pdf" target="_blank">Baixe aqui</a>';