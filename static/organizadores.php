<?php

$title = 'Organizadores';

$content = '<p>O IF Code 2018 é organizado por:</p>';
$content .= '<ul>';
$content .= '<li>Alex Mulattieri Orozco (<a href="http://lattes.cnpq.br/6003314567230770" target="_blank">currículo</a>)</li>';
$content .= '<li>Maurício da Silva Escobar (<a href="http://lattes.cnpq.br/1769915242003623" target="_blank">currículo</a>)</li>';
$content .= '<li>Mônica Xavier Py (<a href="http://lattes.cnpq.br/2255764287952798" target="_blank">currículo</a>)</li>';
$content .= '<li>Matheus Wichman (<a href="http://lattes.cnpq.br/3020575034006816" target="_blank">currículo</a>)</li>';
$content .= '<li>Rodrigo Remor Oliveira (<a href="http://lattes.cnpq.br/1886580308224807" target="_blank">currículo</a>)</li>';
$content .= '</ul>';
