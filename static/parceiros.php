<?php

$title = 'Parceiros';

$content = '<div class="parceiros row">';
$content .= '<div class="parceiros-1 text-center col-md-8 col-md-offset-2">Você sabia que o Brasil possui um déficit de 140 mil profissionais na área de Tecnologia da Informação (TI) e que a previsão é que esse número aumente para 420 mil até 2020?</div>';
$content .= '</div>';

$content .= '<div class="parceiros row">';
$content .= '<div class="parceiros-2 text-center col-md-6 col-md-offset-3">Muitas dessas vagas não são preenchidas por falta de qualificação profissional. Você pode nos ajudar a transformar essa realidade.</div>';
$content .= '</div>';

$content .= '<div class="parceiros row">';
$content .= '<div class="parceiros-3 text-center col-md-6 col-md-offset-3"><a href="'.BASE_PATH.'/seja-parceiro">Seja um parceiro</a> do IF Code e nos ajude formar profissionais cada vez mais capacitados para o mercado de TI, onde você vai poder:</div>';
$content .= '</div>';

$content .= '<div class="parceiros row">';
$content .= '<div class="text-center col-md-3"><span class="glyphicon glyphicon-bullhorn"></span><br>Divulgar sua empresa com banner no evento</div>';
$content .= '<div class="text-center col-md-3"><span class="glyphicon glyphicon-star"></span><br>Distribuir material de divulgação</div>';
$content .= '<div class="text-center col-md-3"><span class="glyphicon glyphicon-thumbs-up"></span><br>Ter a marca de sua empresa em nosso site oficial, material impresso ou confeccionado</div>';
$content .= '<div class="text-center col-md-3"><span class="glyphicon glyphicon-gift"></span><br>Distribuir brindes em sorteios nas solenidades de abertura ou fechamento do evento</div>';
$content .= '</div>';