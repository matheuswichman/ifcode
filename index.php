<?php
session_start();
require_once 'connection.php';
require_once 'subscription/subscription_functions.php';
require_once 'layout/header.php';
?>
<div class="row">
	<div class="col-md-9">
		<h2 class="page-title">IFCODE 2018</h2>
		<p class="text-justify">O IF Code é um evento promovido pelo Instituto Federal Sul-rio-grandense (IFSul) câmpus Sapucaia do Sul que ocorrerá nos dias <strong>29 e 30 de setembro</strong> e que busca incentivar e proporcionar aos alunos da Instituição o desenvolvimento de habilidades de programação e criatividade. Clique <a href="<?= BASE_PATH ?>/sobre">aqui</a> para saber mais sobre o evento.</p>
		<div class="resumo text-center" style="font-size: 16px;">
			<strong>CONFIRA ABAIXO A LISTA DE INSCRIÇÕES HOMOLOGADAS:</strong><br>
			<a href="<?= BASE_PATH ?>/assets/resultado-maratona.pdf" target="_blank">Maratona</a> |
			<a href="<?= BASE_PATH ?>/assets/resultado-hackathon.pdf" target="_blank">Hackathon</a>
		</div>
	</div>
	<div class="col-md-3 text-center sidebar">
		<?php include 'layout/sidebar.php'; ?>
	</div>
</div>
<?php require_once 'layout/footer.php'; ?>
